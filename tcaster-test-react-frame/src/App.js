import React from 'react';
import './App.css';

// We're using bulma.io for example styles
// you can use any css styling as you like
import './main.css';

import engi from './engineer.png';

function App() {
  return (
    <div className="App">
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-8-desktop is-offset-2-desktop">
              <div>
                <img src={engi} alt="engineering" />
              </div>
              <div>
                Add to fav icon:
                <i className="fas fa-lg fa-heart " />
              </div>
              <div>
                Added to fav icon:
                <i className="fas fa-user" />{' '}
              </div>
              <div>
                Share icon:
                <i className="fas fa-share-alt" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
